package movieRatings.api;

import java.net.URL;
import java.util.List;
import java.util.Map;

import com.google.common.base.Joiner;

public class Search {
	private int total;
	private List<Movie> movies;
	private Map<String, URL> links;
	private String linkTemplate;
	
	public int getTotal() {
		return total;
	}

	public List<Movie> getMovies() {
		return movies;
	}

	public Map<String, URL> getLinks() {
		return links;
	}

	public String getLinkTemplate() {
		return linkTemplate;
	}

	@Override
	public String toString() {
		return "Search [total=" + total + ", movies=\n\t" + Joiner.on("\n\t").join(movies) + "\n, links=" + links + ", linkTemplate=" + linkTemplate + "]";
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public void setMovies(List<Movie> movies) {
		this.movies = movies;
	}

	public void setLinks(Map<String, URL> links) {
		this.links = links;
	}

	public void setLinkTemplate(String linkTemplate) {
		this.linkTemplate = linkTemplate;
	}
}
