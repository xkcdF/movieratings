package movieRatings;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import movieRatings.api.Movie;
import movieRatings.api.Search;

import org.apache.http.client.cache.HttpCacheContext;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.cache.CacheConfig;
import org.apache.http.impl.client.cache.CachingHttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;


public class Main {
	private static final String API_KEY = "j6jh98x8ekn3stczqachvq7g";
	
	private static final String SUFFIX = "apikey=" + API_KEY;
	
	private static final String BASE = "http://api.rottentomatoes.com/api/public/v1.0/";
	
	private static final String SEARCH = BASE + "movies.json?page_limit=10&page=1&" + SUFFIX;
	
	// conservatively set to 3 req / sec.
	private static final int MIN_DELAY = 1000 / 3;
	
	private long lastRequest;

	private final CloseableHttpClient client;
	private final HttpCacheContext context = HttpCacheContext.create();
	
	public Main(){
		File cacheDir = new File("cache");
		if(!cacheDir.exists()) cacheDir.mkdir();
		client = CachingHttpClients.custom()
				.setCacheConfig(CacheConfig.DEFAULT)
				.setCacheDir(cacheDir)
				.build();
	}
	
	
	private void throttle() {
		long now = System.currentTimeMillis();
		long diff = lastRequest - now + MIN_DELAY;
		if(diff > 0) {
			try {
				Thread.sleep(diff);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		lastRequest = now;
	}
	
	public Search search(String query) throws IOException, URISyntaxException {
		URI url = new URIBuilder(SEARCH).setParameter("q", query).build();
		return loadJson(new HttpGet(url), Search.class);
	}


	private <T> T loadJson(HttpGet request, Class<T> classOfT) throws IOException {
		throttle();
		
		String json = load(request);
		
		try {
			return getParser().fromJson(json, classOfT);
		} catch(JsonSyntaxException e) {
			System.err.println("Error loading URL: " + request.getURI());
			JsonObject jo;
			try {
				jo = new JsonParser().parse(json).getAsJsonObject();
			} catch(Exception ex) {
				System.err.println("Could not parse as JSON: " + ex);
				System.err.println(json);
				throw ex;
			}
			
			try {
				return getParser().fromJson(jo, classOfT);
			} catch(JsonSyntaxException ex) {
				System.err.println("Could not convert into Search object: " + ex.getMessage());
//				Pattern c = Pattern.compile("at line (\\d+) column (\\d+)$");
//				Matcher m = c.matcher(ex.getMessage());
//				m.find();
//				int line = Integer.parseInt(m.group(1));
//				int col = Integer.parseInt(m.group(1));
				System.err.println(jo);
				throw ex;
			}
		}
	}


	private String load(HttpUriRequest request) throws IOException {
		try(CloseableHttpResponse response = client.execute(request, context)) {
			return EntityUtils.toString(response.getEntity());
		}
	}
	
	public static class MovieInfo {
		public String title;
		public int year;
		private String link;

		public MovieInfo(String title, int year, String link) {
			this.title = title;
			this.year = year;
			this.link = link;
		}
		
		@Override
		public String toString() {
			return title + " (" + year + ")";
		}
	}
	
	/**
	 * Finds the movie with the specified title
	 */
	public Movie find(MovieInfo info) throws IOException, URISyntaxException {
		List<Movie> candidates = search(info.title).getMovies();
		
		if(candidates == null) {
			candidates = search(info.title).getMovies();
			if(candidates == null) {
				return null;
			}
		}
		
		if(candidates.size() == 1) {
			return candidates.get(0);
		}
		
		for(Movie movie : candidates) {
			if(info.title.equalsIgnoreCase(movie.getTitle()) && Math.abs(info.year - movie.getYear()) <= 1)
				return movie;
		}
		
		// if it didn't work, just take one from the same year
		for(Movie movie : candidates) {
			if(Math.abs(info.year - movie.getYear()) <= 1)
				return movie;
		}
		
		
//		System.out.println("Could not find " + info + " (" + candidates.size() + " candidates: " + candidates + ")");
		return null;
	}
	
	public void listDisney(int limit, int ratingThreshold) throws IOException, URISyntaxException {
		Document doc = Jsoup.parse(load(new HttpGet("https://en.wikipedia.org/wiki/List_of_Disney_theatrical_animated_features")));
		
		Elements rows = doc.getElementById("Released").parent().nextElementSibling().select("tr");
		List<MovieInfo> movies = new ArrayList<>(Math.min(rows.size() - 1, limit));
		for(Element row : rows.subList(Math.max(1, rows.size() - limit), rows.size())) {
			Elements children = row.children();
			String title = children.get(0).text();
			String yearS = children.get(1).text().substring(1, 5);
			int year = Integer.parseInt(yearS);
			String link = "https://en.wikipedia.org" + children.get(0).select("a").attr("href");
			movies.add(new MovieInfo(title, year, link));
		}
		
		Collections.reverse(movies);

		StringBuilder output = new StringBuilder("<html><head><title>Disney Movies</title><body><table><tr><th>Title</th><th>Critics</th><th>Audience</th><th>Reviews</th><th>Pic</th></tr>");
		for(MovieInfo m : movies)
			pprint(find(m), m, ratingThreshold, output);
		
		output.append("</table></body></html>");
		File outFile = new File("output.html");
		Files.write(output, outFile, Charsets.UTF_8);
		Desktop.getDesktop().open(outFile);
	}

	private Gson getParser() {
		return new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).registerTypeAdapter(Integer.TYPE, INTEGER).create();
	}
	
	private static void pprint(Movie m, MovieInfo info, int ratingThreshold, StringBuilder output) {
		if(m != null && m.getRatings() != null && m.getRatings().getCriticsScore() > ratingThreshold) {
			System.out.printf("%-60s | %3s | %3s | %s \n",
					m.getTitle() + " (" + m.getYear() + ")",
					m.getRatings().getCriticsScore(),
					m.getRatings().getAudienceScore(),
				    m.getLinks().get("alternate"));
			output.append("<tr><td><a href=\"").append(info.link).append("\">").append(m.getTitle()).append(" (").append(m.getYear()).append(")</a></td>");
			output.append("<td>").append(m.getRatings().getCriticsScore()).append("</td>");
			output.append("<td>").append(m.getRatings().getAudienceScore()).append("</td>");
			output.append("<td><a href=\"").append(m.getLinks().get("alternate")).append("\">RottenTomatoes</a></td>");
			output.append("<td><a href=\"").append(m.getPosters().get("original")).append("\"><img src=\"").append(m.getPosters().get("thumbnail")).append("\" /></a></td></tr>");
		}
	}
	
	public static void main(String[] args) throws IOException, URISyntaxException {
		new Main().listDisney(1000, 60);
	}
	
	private static TypeAdapter<Integer> INTEGER = new TypeAdapter<Integer>() {

        @Override
        public void write(JsonWriter out, Integer value)
                throws IOException {
            out.value(value);
        }

        @Override
        public Integer read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return 0;
            }
            try {
                String result = in.nextString();
                if ("".equals(result)) {
                    return 0;
                }
                return Integer.parseInt(result);
            } catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
        }
    };
}
