package movieRatings.api;

public class Rating {
	String criticsRating;
	int criticsScore;
	String audienceRating;
	int audienceScore;
	
	
	@Override
	public String toString() {
		return "Rating [criticsRating=" + criticsRating + ", criticsScore=" + criticsScore + ", audienceRating=" + audienceRating + ", audienceScore=" + audienceScore + "]";
	}


	public String getCriticsRating() {
		return criticsRating;
	}


	public void setCriticsRating(String criticsRating) {
		this.criticsRating = criticsRating;
	}


	public int getCriticsScore() {
		return criticsScore;
	}


	public void setCriticsScore(int criticsScore) {
		this.criticsScore = criticsScore;
	}


	public String getAudienceRating() {
		return audienceRating;
	}


	public void setAudienceRating(String audienceRating) {
		this.audienceRating = audienceRating;
	}


	public int getAudienceScore() {
		return audienceScore;
	}


	public void setAudienceScore(int audienceScore) {
		this.audienceScore = audienceScore;
	}
}
