package movieRatings.api;

import java.net.URL;
import java.util.List;
import java.util.Map;

public class Movie {
	String id;
	String title;
	int year;
	String mpaaRating;
	int runtime;
	String criticsConsensus;
	Map<String, String> releaseDates;
	Rating ratings;
	String synopsis;
	Map<String, URL> posters;
	List<Cast> abridgedCast;
	
	Map<String, String> alternateIds;
	Map<String, URL> links;
	
	@Override
	public String toString() {
		return String.format("%s (%s) %s", title, year, releaseDates);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getMpaaRating() {
		return mpaaRating;
	}

	public void setMpaaRating(String mpaaRating) {
		this.mpaaRating = mpaaRating;
	}

	public int getRuntime() {
		return runtime;
	}

	public void setRuntime(int runtime) {
		this.runtime = runtime;
	}

	public String getCriticsConsensus() {
		return criticsConsensus;
	}

	public void setCriticsConsensus(String criticsConsensus) {
		this.criticsConsensus = criticsConsensus;
	}

	public Map<String, String> getReleaseDates() {
		return releaseDates;
	}

	public void setReleaseDates(Map<String, String> releaseDates) {
		this.releaseDates = releaseDates;
	}

	public Rating getRatings() {
		return ratings;
	}

	public void setRatings(Rating ratings) {
		this.ratings = ratings;
	}

	public String getSynopsis() {
		return synopsis;
	}

	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}

	public Map<String, URL> getPosters() {
		return posters;
	}

	public void setPosters(Map<String, URL> posters) {
		this.posters = posters;
	}

	public List<Cast> getAbridgedCast() {
		return abridgedCast;
	}

	public void setAbridgedCast(List<Cast> abridgedCast) {
		this.abridgedCast = abridgedCast;
	}

	public Map<String, String> getAlternateIds() {
		return alternateIds;
	}

	public void setAlternateIds(Map<String, String> alternateIds) {
		this.alternateIds = alternateIds;
	}

	public Map<String, URL> getLinks() {
		return links;
	}

	public void setLinks(Map<String, URL> links) {
		this.links = links;
	}
}
