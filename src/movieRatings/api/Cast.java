package movieRatings.api;

import java.util.List;

public class Cast {
	private String name;
	private String id;
	private List<String> characters;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getCharacters() {
		return characters;
	}

	public void setCharacters(List<String> characters) {
		this.characters = characters;
	}

	@Override
	public String toString() {
		return "Cast [name=" + name + ", id=" + id + ", characters=" + characters + "]";
	}
	
}
