package movieRatings;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.JOptionPane;

public class TestImgBomb {
	public static void main(String[] args) throws IOException {
		long start = System.nanoTime();
		File input = new File("C:\\Temp\\p1g.png");
		
		try(ImageInputStream in = ImageIO.createImageInputStream(input)) {
		    final Iterator<ImageReader> readers = ImageIO.getImageReaders(in);
		    if (readers.hasNext()) {
		        ImageReader reader = readers.next();
		        try {
		            reader.setInput(in);
		    		System.out.println(reader.getWidth(0) + " / " + reader.getHeight(0));
		        } finally {
		            reader.dispose();
		        }
		    }
		}
		
		JOptionPane.showMessageDialog(null, String.format("done in %s seconds", new DecimalFormat("0.###").format((System.nanoTime() - start) * 1e-9)));
	}
}
